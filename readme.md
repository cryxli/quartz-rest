# quartz-rest

[quartz](http://www.quartz-scheduler.org) is a powerful task scheduler for java. This projects adds a REST interface to manage quartz ``Scheduler``s remotely.

## Basics

The little library ``li.cryx.quartz:quartz-core`` is based on Spring 5 and Quartz 2. Just add a component scan for the package ``li.cryx.quartz`` to your application configuration after you included the artifact:

```
@ComponentScan("li.cryx.quartz")
@SpringBootApplication
public class DemoApplication {

	public static void main(final String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
```

Spring boot will expose all quartz schedulers to the REST controller which in turn will expose common methods and properties through JSON.

Note: You have to secure the controller yourself!

## Interface

There is a [swagger.yaml](https://bitbucket.org/cryxli/quartz-rest/src/master/quartz-core/swagger.yaml) documentation that should explain all the requests of the REST interface.

I use [JMeter 4](https://jmeter.apache.org) to test the REST calls. My [test file](https://bitbucket.org/cryxli/quartz-rest/src/master/quartz-testing/jmeter4.jmx) is also included in the test project.

## Progress

In its current state, this project is a prove of concept. 

So far you can inspect pretty much everything that goes on in a quartz scheduler, but you do not yet have all the methods provided to manipulate the scheduler's internal state. Like creating new triggers and altering existing ones, etc.
