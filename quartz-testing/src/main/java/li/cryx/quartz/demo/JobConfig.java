package li.cryx.quartz.demo;

import java.time.Instant;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JobConfig {

	// job without a trigger
	@Bean
	public JobDetail hourJobDetail() {
		return JobBuilder.newJob(HourTickTask.class).withIdentity("hourTick")
				.storeDurably().build();
	}

	// job is a simple trigger
	@Bean
	public JobDetail minuteJobDetail() {
		return JobBuilder.newJob(MinuteTickTask.class)
				.withIdentity("minuteTick").storeDurably().build();
	}

	// a simple trigger
	@Bean
	public Trigger minuteJobTrigger() {
		return TriggerBuilder.newTrigger().forJob(minuteJobDetail())
				.withIdentity("minuteTrigger")
				.withSchedule(SimpleScheduleBuilder.simpleSchedule()
						.withIntervalInMinutes(1).repeatForever())
				.startAt(java.util.Date.from(Instant.ofEpochSecond(0))).build();
	}

	// job with a cron trigger
	@Bean
	public JobDetail secondJobDetail() {
		return JobBuilder.newJob(SecondTickTask.class)
				.withIdentity("secondTick").storeDurably().build();
	}

	// a cron trigger
	@Bean
	public Trigger secondJobTrigger() {
		return TriggerBuilder.newTrigger().forJob(secondJobDetail())
				.withIdentity("secondTrigger")
				.withSchedule(CronScheduleBuilder.cronSchedule("0/1 * * * * ?"))
				.build();
	}

}
