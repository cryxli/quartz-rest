package li.cryx.quartz.demo;

import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import li.cryx.quartz.listener.JobExecutionLogger;

@Configuration
public class JobListenerConfig {

	@Bean
	public JobListener jobExecutionListener(final Scheduler sched)
			throws SchedulerException {
		final JobExecutionLogger jobLogger = new JobExecutionLogger(sched);
		jobLogger.setStartingLogLevel(null);
		return jobLogger;
	}

}
