package li.cryx.quartz.demo;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

@DisallowConcurrentExecution
public class MinuteTickTask extends QuartzJobBean {

	@Override
	protected void executeInternal(final JobExecutionContext context) {
		System.out.println("...minute...");
	}

}
