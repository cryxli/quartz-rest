package li.cryx.quartz.demo;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

@DisallowConcurrentExecution
public class HourTickTask extends QuartzJobBean {

	@Override
	protected void executeInternal(final JobExecutionContext context)
			throws JobExecutionException {
		System.out.println("...hour...");
	}

}
