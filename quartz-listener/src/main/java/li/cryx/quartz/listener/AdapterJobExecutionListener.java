package li.cryx.quartz.listener;

import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This adapter implements the <code>JobListener</code> and offers four
 * protected methods to implementing classes to react to job events.
 *
 * <ul>
 * <li>{@link #jobVetoed(Scheduler, JobDetail, Trigger, JobExecutionContext)}</li>
 * <li>{@link #jobStarting(Scheduler, JobDetail, Trigger, JobExecutionContext)}</li>
 * <li>{@link #jobFinished(Scheduler, JobDetail, Trigger, JobExecutionContext)}</li>
 * <li>{@link #jobFailed(Scheduler, JobDetail, Trigger, JobExecutionException, JobExecutionContext)}</li>
 * </ul>
 *
 * @author cryxli
 */
public abstract class AdapterJobExecutionListener implements JobListener {

	private static final Logger LOG = LoggerFactory
			.getLogger(AdapterJobExecutionListener.class);

	protected AdapterJobExecutionListener() {
	}

	protected AdapterJobExecutionListener(final Scheduler sched)
			throws SchedulerException {
		registerWithScheduler(sched);
	}

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

	@Override
	public final void jobExecutionVetoed(final JobExecutionContext context) {
		try {
			jobVetoed( //
					context.getScheduler(), //
					context.getJobDetail(), //
					context.getTrigger(), //
					context //
			);
		} catch (SchedulerException e) {
			LOG.error("Error accesing scheduler", e);
		}
	}

	protected void jobFailed(final Scheduler sched, final JobDetail jobDetail,
			final Trigger trigger, final JobExecutionException jobException,
			final JobExecutionContext context) throws SchedulerException {
		// do nothing
	}

	protected void jobFinished(final Scheduler sched, final JobDetail jobDetail,
			final Trigger trigger, final JobExecutionContext context)
			throws SchedulerException {
		// do nothing
	}

	protected void jobStarting(final Scheduler sched, final JobDetail jobDetail,
			final Trigger trigger, final JobExecutionContext context)
			throws SchedulerException {
		// do nothing
	}

	@Override
	public final void jobToBeExecuted(final JobExecutionContext context) {
		try {
			jobStarting( //
					context.getScheduler(), //
					context.getJobDetail(), //
					context.getTrigger(), //
					context //
			);
		} catch (SchedulerException e) {
			LOG.error("Error accesing scheduler", e);
		}
	}

	protected void jobVetoed(final Scheduler sched, final JobDetail jobDetail,
			final Trigger trigger, final JobExecutionContext context)
			throws SchedulerException {
		// do nothing
	}

	@Override
	public final void jobWasExecuted(final JobExecutionContext context,
			final JobExecutionException jobException) {
		try {
			if (jobException == null) {
				jobFinished( //
						context.getScheduler(), //
						context.getJobDetail(), //
						context.getTrigger(), //
						context //
				);
			} else {
				jobFailed( //
						context.getScheduler(), //
						context.getJobDetail(), //
						context.getTrigger(), //
						jobException, //
						context //
				);
			}
		} catch (SchedulerException e) {
			LOG.error("Error accesing scheduler", e);
		}
	}

	public void registerWithScheduler(final Scheduler sched)
			throws SchedulerException {
		// register to all jobs on the scheduler
		sched.getListenerManager().addJobListener(this,
				GroupMatcher.anyJobGroup());
	}

}
