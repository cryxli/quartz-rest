package li.cryx.quartz.listener;

import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import li.cryx.quartz.exception.QuartzExceptionWrapper;

/**
 * This class is a <code>JobListener</code> that will log all intercepted events
 * to the application log.
 *
 * @author cryxli
 */
public class JobExecutionLogger extends AdapterJobExecutionListener {

	private static final Logger LOG = LoggerFactory
			.getLogger(JobExecutionLogger.class);

	private Level startingLogLevel = Level.INFO;

	private Level vetoedLogLevel = Level.INFO;

	private Level finishedLogLevel = Level.INFO;

	private Level failedLogLevel = Level.WARN;

	public JobExecutionLogger(final Scheduler sched) throws SchedulerException {
		super(sched);
	}

	@Override
	protected void jobFailed(final Scheduler sched, final JobDetail jobDetail,
			final Trigger trigger, final JobExecutionException jobException,
			final JobExecutionContext context) throws SchedulerException {
		log(failedLogLevel, "Job {} on {} failed with {}", jobDetail.getKey(),
				sched.getSchedulerName(),
				QuartzExceptionWrapper.exceptionToString(jobException));
	}

	@Override
	protected void jobFinished(final Scheduler sched, final JobDetail jobDetail,
			final Trigger trigger, final JobExecutionContext context)
			throws SchedulerException {
		log(finishedLogLevel, "Job {} on {} finished in {} ms",
				jobDetail.getKey(), sched.getSchedulerName(),
				context.getJobRunTime());
	}

	@Override
	protected void jobStarting(final Scheduler sched, final JobDetail jobDetail,
			final Trigger trigger, final JobExecutionContext context)
			throws SchedulerException {
		log(startingLogLevel, "Job {} on {} is starting", jobDetail.getKey(),
				sched.getSchedulerName());
	}

	@Override
	protected void jobVetoed(final Scheduler sched, final JobDetail jobDetail,
			final Trigger trigger, final JobExecutionContext context)
			throws SchedulerException {
		log(vetoedLogLevel, "Job {} on {} not started due to veto",
				jobDetail.getKey(), sched.getSchedulerName());
	}

	private void log(final Level level, final String msg,
			final Object... arguments) {
		// only using setters 'level' cannot be null
		switch (level) {
			case DEBUG :
				LOG.debug(msg, arguments);
				break;
			case INFO :
				LOG.info(msg, arguments);
				break;
			case WARN :
				LOG.warn(msg, arguments);
				break;
			case ERROR :
				LOG.error(msg, arguments);
				break;
			default :
				LOG.trace(msg, arguments);
				break;
		}
	}

	public void setFailedLogLevel(final Level level) {
		failedLogLevel = level != null ? level : Level.TRACE;
	}

	public void setFinishedLogLevel(final Level level) {
		finishedLogLevel = level != null ? level : Level.TRACE;
	}

	public void setStartingLogLevel(final Level level) {
		startingLogLevel = level != null ? level : Level.TRACE;
	}

	public void setVetoedLogLevel(final Level level) {
		vetoedLogLevel = level != null ? level : Level.TRACE;
	}

}
