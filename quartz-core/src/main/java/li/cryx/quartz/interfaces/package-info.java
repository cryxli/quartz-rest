/**
 * Since a quartz scheduler offers so many methods and functions about its
 * internals the corresponding REST methods are split into different sections
 * each represented by an interface in this package.
 */
package li.cryx.quartz.interfaces;