package li.cryx.quartz.interfaces;

import java.util.Set;

import li.cryx.quartz.exception.SchedulerNotFoundException;
import li.cryx.quartz.exception.WrappedQuartzException;
import li.cryx.quartz.model.SchedulerInfo;

public interface RestQuartzSchedulerInterface {

	SchedulerInfo getSchedulerInfo(final String schedulerName)
			throws SchedulerNotFoundException, WrappedQuartzException;

	Set<String> getSchedulerNames() throws WrappedQuartzException;

	boolean isRunning(final String schedulerName)
			throws SchedulerNotFoundException, WrappedQuartzException;

	boolean isShutdown(final String schedulerName)
			throws SchedulerNotFoundException, WrappedQuartzException;

	boolean pauseScheduler(final String schedulerName)
			throws SchedulerNotFoundException, WrappedQuartzException;

	boolean shutdownScheduler(final String schedulerName)
			throws SchedulerNotFoundException, WrappedQuartzException;

	boolean startScheduler(final String schedulerName)
			throws SchedulerNotFoundException, WrappedQuartzException;

}
