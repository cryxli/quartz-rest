package li.cryx.quartz.interfaces;

import java.util.Set;

import li.cryx.quartz.exception.SchedulerNotFoundException;
import li.cryx.quartz.exception.TriggerNotFoundException;
import li.cryx.quartz.exception.WrappedQuartzException;
import li.cryx.quartz.model.TriggerInfo;

public interface RestQuartzTriggerInterface {

	Set<TriggerInfo> getAllTriggers(final String schedulerName)
			throws SchedulerNotFoundException, WrappedQuartzException;

	TriggerInfo getTriggerInfo(final String schedulerName, final String group,
			final String name) throws TriggerNotFoundException,
			SchedulerNotFoundException, WrappedQuartzException;

	void pauseTrigger(final String schedulerName, final String group,
			final String name) throws SchedulerNotFoundException,
			TriggerNotFoundException, WrappedQuartzException;

	void removeTrigger(final String schedulerName, final String group,
			final String name) throws SchedulerNotFoundException,
			TriggerNotFoundException, WrappedQuartzException;

	void resumeTrigger(final String schedulerName, final String group,
			final String name) throws SchedulerNotFoundException,
			TriggerNotFoundException, WrappedQuartzException;

}
