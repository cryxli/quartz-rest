package li.cryx.quartz.interfaces;

import java.util.Set;

import li.cryx.quartz.exception.SchedulerNotFoundException;
import li.cryx.quartz.exception.WrappedQuartzException;
import li.cryx.quartz.model.TaskInfo;
import li.cryx.quartz.model.TriggerConfig;

public interface RestQuartzTaskInterface {

	Set<TaskInfo> getAllTasks(final String schedulerName)
			throws SchedulerNotFoundException, WrappedQuartzException;

	TaskInfo getTaskInfo(final String schedulerName, final String triggerGroup,
			final String triggerName)
			throws SchedulerNotFoundException, WrappedQuartzException;

	TaskInfo scheduleJob(String schedulerName, String jobGroup, String jobName,
			TriggerConfig trigger)
			throws SchedulerNotFoundException, WrappedQuartzException;

}
