package li.cryx.quartz.interfaces;

import java.util.Set;

import li.cryx.quartz.exception.JobNotFoundException;
import li.cryx.quartz.exception.SchedulerNotFoundException;
import li.cryx.quartz.exception.WrappedQuartzException;
import li.cryx.quartz.model.JobInfo;

/**
 * <code>Job</code> and therefore <code>JobDetail</code> related methods on the
 * REST interface.
 *
 * @author cryxli
 */
public interface RestQuartzJobInterface {

	/**
	 * Get information about all jobs registered with a certain scheduler.
	 *
	 * @param schedulerName
	 *            Name of a scheduler.
	 * @return A list of objects describing the jobs as seen by quartz.
	 * @throws SchedulerNotFoundException
	 *             If the indicated scheduler was not found.
	 * @throws WrappedQuartzException
	 *             If quartz could not process the request.
	 */
	Set<JobInfo> getAllJobs(final String schedulerName)
			throws SchedulerNotFoundException, WrappedQuartzException;

	/**
	 * Get information about a certain job.
	 *
	 * @param schedulerName
	 *            Name of a scheduler.
	 * @param group
	 *            Group of a job.
	 * @param name
	 *            Name of a job.
	 * @return An object describing the job as seen by quartz.
	 * @throws SchedulerNotFoundException
	 *             If the indicated scheduler was not found.
	 * @throws JobNotFoundException
	 *             If the indicated job was not found.
	 * @throws WrappedQuartzException
	 *             If quartz could not process the request.
	 */
	JobInfo getJobInfo(final String schedulerName, final String group,
			final String name) throws SchedulerNotFoundException,
			JobNotFoundException, WrappedQuartzException;

	/**
	 * Deletes a job from the scheduler. Cascadingly removes all triggers that
	 * pointed to the job as their target got invalidated.
	 *
	 * @param schedulerName
	 *            Name of a scheduler.
	 * @param group
	 *            Group of a job.
	 * @param name
	 *            Name of a job.
	 * @throws SchedulerNotFoundException
	 *             If the indicated scheduler was not found.
	 * @throws JobNotFoundException
	 *             If the indicated job was not found.
	 * @throws WrappedQuartzException
	 *             If quartz could not process the request.
	 */
	void removeJob(final String schedulerName, final String group,
			final String name) throws SchedulerNotFoundException,
			JobNotFoundException, WrappedQuartzException;

	/**
	 * Have quartz run a job once. This is achieved by creating a temporal
	 * trigger that will fire immediately and then remove itself from the
	 * scheduler again.
	 *
	 * @param schedulerName
	 *            Name of a scheduler.
	 * @param group
	 *            Group of a job.
	 * @param name
	 *            Name of a job.
	 * @throws SchedulerNotFoundException
	 *             If the indicated scheduler was not found.
	 * @throws JobNotFoundException
	 *             If the indicated job was not found.
	 * @throws WrappedQuartzException
	 *             If quartz could not process the request.
	 */
	void runOnce(final String schedulerName, final String group,
			final String name) throws SchedulerNotFoundException,
			JobNotFoundException, WrappedQuartzException;

}
