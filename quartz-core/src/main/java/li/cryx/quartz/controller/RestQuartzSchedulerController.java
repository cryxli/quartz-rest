package li.cryx.quartz.controller;

import java.util.Set;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import li.cryx.quartz.builder.SchedulerInfoBuilder;
import li.cryx.quartz.exception.QuartzExceptionWrapper;
import li.cryx.quartz.interfaces.RestQuartzSchedulerInterface;
import li.cryx.quartz.model.SchedulerInfo;

/**
 * Spring MVC REST controller to expose properties and methods of Quartz
 * schedulers related to schedulers themselves.
 *
 * @author cryxli
 * @since 0.1
 */
@CrossOrigin
@RestController
@RequestMapping("/api/quartz")
public class RestQuartzSchedulerController extends AbstractRestQuartzController
		implements
			RestQuartzSchedulerInterface {

	@Override
	@GetMapping(path = "/{schedulerName}/info")
	public @ResponseBody SchedulerInfo getSchedulerInfo(
			@PathVariable("schedulerName") final String schedulerName) {
		try {
			final Scheduler sched = getScheduler(schedulerName);
			return SchedulerInfoBuilder.fromScheduler(sched);
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	@Override
	@GetMapping(path = "/schedulers")
	public @ResponseBody Set<String> getSchedulerNames() {
		return schedulerNames();
	}

	@Override
	@GetMapping(path = "/{schedulerName}/started")
	public @ResponseBody boolean isRunning(
			@PathVariable("schedulerName") final String schedulerName) {
		try {
			return SchedulerInfoBuilder.isRunning(getScheduler(schedulerName));
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	@Override
	@GetMapping(path = "/{schedulerName}/shutdown")
	public boolean isShutdown(
			@PathVariable("schedulerName") final String schedulerName) {
		try {
			return getScheduler(schedulerName).isShutdown();
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	@Override
	@PutMapping(path = "/{schedulerName}/pause")
	public @ResponseBody boolean pauseScheduler(
			@PathVariable("schedulerName") final String schedulerName) {
		try {
			final Scheduler sched = getScheduler(schedulerName);
			sched.standby();
			return sched.isInStandbyMode();
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	@Override
	@PutMapping(path = "/{schedulerName}/shutdown")
	public @ResponseBody boolean shutdownScheduler(
			@PathVariable("schedulerName") final String schedulerName) {
		try {
			final Scheduler sched = getScheduler(schedulerName);
			if (!sched.isShutdown()) {
				sched.shutdown();
			}
			return sched.isShutdown();
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	@Override
	@PutMapping(path = "/{schedulerName}/start")
	public @ResponseBody boolean startScheduler(
			@PathVariable("schedulerName") final String schedulerName) {
		try {
			final Scheduler sched = getScheduler(schedulerName);
			if (!SchedulerInfoBuilder.isRunning(sched)) {
				sched.start();
			}
			return SchedulerInfoBuilder.isRunning(sched);
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

}
