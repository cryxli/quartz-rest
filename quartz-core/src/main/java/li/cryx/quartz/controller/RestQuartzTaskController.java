package li.cryx.quartz.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import li.cryx.quartz.builder.TaskInfoBuilder;
import li.cryx.quartz.exception.JobNotFoundException;
import li.cryx.quartz.exception.QuartzExceptionWrapper;
import li.cryx.quartz.exception.SchedulerNotFoundException;
import li.cryx.quartz.exception.TriggerConfigException;
import li.cryx.quartz.exception.WrappedQuartzException;
import li.cryx.quartz.interfaces.RestQuartzTaskInterface;
import li.cryx.quartz.model.TaskInfo;
import li.cryx.quartz.model.TriggerConfig;
import li.cryx.quartz.model.TriggerType;

/**
 * Spring MVC REST controller to expose properties and methods of Quartz
 * schedulers related to tasks.
 *
 * <p>
 * A task is not a quartz construct thatn consists of a job with an assigned
 * trigger.
 * </p>
 *
 * @author cryxli
 * @since 0.1
 */
@CrossOrigin
@RestController
@RequestMapping("/api/quartz")
public class RestQuartzTaskController extends AbstractRestQuartzController
		implements
			RestQuartzTaskInterface {

	@Override
	@GetMapping(path = "/{schedulerName}/tasks")
	public @ResponseBody Set<TaskInfo> getAllTasks(
			@PathVariable("schedulerName") final String schedulerName) {
		final Set<TaskInfo> tasks = new HashSet<>();
		final Scheduler sched = getScheduler(schedulerName);
		try {
			// look for existing jobs
			for (JobKey jobKey : sched.getJobKeys(GroupMatcher.anyJobGroup())) {
				final List<? extends Trigger> triggersOfJob = sched
						.getTriggersOfJob(jobKey);
				// only jobs with triggers are tasks
				for (Trigger trigger : triggersOfJob) {
					tasks.add(TaskInfoBuilder.fromScheduler(sched)
							.trigger(trigger).build());
				}
			}
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
		return tasks;
	}

	@Override
	@GetMapping(path = "/{schedulerName}/task/{triggerGroup}/{triggerName}")
	public @ResponseBody TaskInfo getTaskInfo(
			@PathVariable("schedulerName") final String schedulerName,
			@PathVariable("triggerGroup") final String triggerGroup,
			@PathVariable("triggerName") final String triggerName) {
		return TaskInfoBuilder.fromScheduler(getScheduler(schedulerName))
				.trigger(triggerName, triggerGroup).build();
	}

	@Override
	@PostMapping(path = "/{schedulerName}/job/{jobGroup}/{jobName}/trigger", consumes = MimeTypeUtils.APPLICATION_JSON_VALUE)
	public @ResponseBody TaskInfo scheduleJob(
			@PathVariable("schedulerName") final String schedulerName,
			@PathVariable("jobGroup") final String jobGroup,
			@PathVariable("jobName") final String jobName,
			@RequestBody final TriggerConfig trigger)
			throws SchedulerNotFoundException, WrappedQuartzException {
		// check scheduler
		final Scheduler sched = getScheduler(schedulerName);

		// check job
		final JobKey jobKey = new JobKey(jobName, jobGroup);
		try {
			if (sched.getJobDetail(jobKey) == null) {
				throw new JobNotFoundException(jobKey);
			}
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}

		// create trigger
		final TriggerKey triggerKey = trigger.getKey(jobKey);
		final TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder
				.newTrigger().forJob(jobKey).withIdentity(triggerKey);
		if (trigger.getType() == TriggerType.CRON) {
			try {
				triggerBuilder.withSchedule(
						CronScheduleBuilder.cronSchedule(trigger.getCron()));
			} catch (RuntimeException e) {
				throw new TriggerConfigException(
						"Not a CRON expression: " + trigger.getCron(),
						e.getCause());
			}
		} else if (trigger.getType() == TriggerType.SIMPLE) {
			triggerBuilder
					.withSchedule(SimpleScheduleBuilder.simpleSchedule()
							.withRepeatCount(trigger.getRepeatCount())
							.withIntervalInMilliseconds(trigger.getInterval()))
					.startAt(trigger.getStartTime());
		} else {
			throw new TriggerConfigException(
					"Unknown trigger type or incomplete trigger definition.");
		}

		// register trigger
		try {
			sched.scheduleJob(triggerBuilder.build());
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}

		// return new task info
		return getTaskInfo(schedulerName, triggerKey.getGroup(),
				triggerKey.getName());
	}

}
