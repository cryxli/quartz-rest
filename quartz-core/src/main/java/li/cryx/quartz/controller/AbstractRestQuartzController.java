package li.cryx.quartz.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;

import li.cryx.quartz.exception.SchedulerNotFoundException;

public class AbstractRestQuartzController {

	/** List of existing schedulers. */
	private final Map<String, Scheduler> schedulers = new HashMap<>();

	protected Scheduler getScheduler(final String schedulerName)
			throws SchedulerNotFoundException {
		if (schedulers.containsKey(schedulerName)) {
			return schedulers.get(schedulerName);
		} else {
			throw new SchedulerNotFoundException(schedulerName);
		}
	}

	protected Set<String> schedulerNames() {
		return schedulers.keySet();
	}

	@Autowired
	public void setSchedulers(final Scheduler[] schedulers)
			throws SchedulerException {
		this.schedulers.clear();
		for (Scheduler s : schedulers) {
			this.schedulers.put(s.getSchedulerName(), s);
		}
	}

}
