package li.cryx.quartz.controller;

import java.util.HashSet;
import java.util.Set;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import li.cryx.quartz.builder.TriggerInfoBuilder;
import li.cryx.quartz.exception.QuartzExceptionWrapper;
import li.cryx.quartz.interfaces.RestQuartzTriggerInterface;
import li.cryx.quartz.model.TriggerInfo;

/**
 * Spring MVC REST controller to expose properties and methods of Quartz
 * schedulers related to triggers.
 *
 * @author cryxli
 * @since 0.1
 */
@CrossOrigin
@RestController
@RequestMapping("/api/quartz")
public class RestQuartzTriggerController extends AbstractRestQuartzController
		implements
			RestQuartzTriggerInterface {

	@Override
	@GetMapping(path = "/{schedulerName}/triggers")
	public @ResponseBody Set<TriggerInfo> getAllTriggers(
			@PathVariable("schedulerName") final String schedulerName) {
		try {
			final Set<TriggerInfo> info = new HashSet<>();
			final Scheduler sched = getScheduler(schedulerName);
			for (TriggerKey triggerKey : sched
					.getTriggerKeys(GroupMatcher.anyTriggerGroup())) {
				info.add(TriggerInfoBuilder.fromScheduler(sched)
						.trigger(triggerKey).build());
			}
			return info;
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	@Override
	@GetMapping(path = "/{schedulerName}/trigger/{group}/{name}")
	public @ResponseBody TriggerInfo getTriggerInfo(
			@PathVariable("schedulerName") final String schedulerName,
			@PathVariable("group") final String group,
			@PathVariable("name") final String name) {
		return TriggerInfoBuilder.fromScheduler(getScheduler(schedulerName))
				.trigger(name, group).build();
	}

	@Override
	@PutMapping(path = {"/{schedulerName}/trigger/{group}/{name}/pause",
			"/{schedulerName}/task/{group}/{name}/pause"})
	public @ResponseBody void pauseTrigger(
			@PathVariable("schedulerName") final String schedulerName,
			@PathVariable("group") final String group,
			@PathVariable("name") final String name) {
		try {
			getScheduler(schedulerName)
					.pauseTrigger(new TriggerKey(name, group));
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	@Override
	@DeleteMapping(path = "/{schedulerName}/trigger/{group}/{name}")
	public void removeTrigger(
			@PathVariable("schedulerName") final String schedulerName,
			@PathVariable("group") final String group,
			@PathVariable("name") final String name) {
		try {
			final Scheduler sched = getScheduler(schedulerName);
			final TriggerKey triggerKey = new TriggerKey(name, group);
			sched.unscheduleJob(triggerKey);
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	@Override
	@PutMapping(path = {"/{schedulerName}/trigger/{group}/{name}/resume",
			"/{schedulerName}/task/{group}/{name}/resume"})
	public @ResponseBody void resumeTrigger(
			@PathVariable("schedulerName") final String schedulerName,
			@PathVariable("group") final String group,
			@PathVariable("name") final String name) {
		try {
			getScheduler(schedulerName)
					.resumeTrigger(new TriggerKey(name, group));
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

}
