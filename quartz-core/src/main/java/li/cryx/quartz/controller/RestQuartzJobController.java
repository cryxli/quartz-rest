package li.cryx.quartz.controller;

import java.util.HashSet;
import java.util.Set;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import li.cryx.quartz.builder.JobInfoBuilder;
import li.cryx.quartz.exception.JobNotFoundException;
import li.cryx.quartz.exception.QuartzExceptionWrapper;
import li.cryx.quartz.interfaces.RestQuartzJobInterface;
import li.cryx.quartz.model.JobInfo;

/**
 * Spring MVC REST controller to expose properties and methods of Quartz
 * schedulers related to jobs.
 *
 * @author cryxli
 * @since 0.1
 */
@CrossOrigin
@RestController
@RequestMapping("/api/quartz")
public class RestQuartzJobController extends AbstractRestQuartzController
		implements
			RestQuartzJobInterface {

	@Override
	@GetMapping(path = "/{schedulerName}/jobs")
	public @ResponseBody Set<JobInfo> getAllJobs(
			@PathVariable("schedulerName") final String schedulerName) {
		try {
			final Scheduler sched = getScheduler(schedulerName);
			final Set<JobInfo> info = new HashSet<>();
			for (JobKey jobKey : sched.getJobKeys(GroupMatcher.anyJobGroup())) {
				info.add(JobInfoBuilder.fromScheduler(sched).job(jobKey)
						.build());
			}
			return info;
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	@Override
	@GetMapping(path = "/{schedulerName}/job/{group}/{name}")
	public @ResponseBody JobInfo getJobInfo(
			@PathVariable("schedulerName") final String schedulerName,
			@PathVariable("group") final String group,
			@PathVariable("name") final String name) {
		return JobInfoBuilder.fromScheduler(getScheduler(schedulerName))
				.job(name, group).build();
	}

	@Override
	@DeleteMapping(path = "/{schedulerName}/job/{group}/{name}")
	public void removeJob(
			@PathVariable("schedulerName") final String schedulerName,
			@PathVariable("group") final String group,
			@PathVariable("name") final String name) {
		try {
			final Scheduler sched = getScheduler(schedulerName);
			final JobKey jobKey = new JobKey(name, group);
			sched.deleteJob(jobKey);
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	@Override
	@PutMapping(path = "/{schedulerName}/job/{group}/{name}/run")
	public void runOnce(
			@PathVariable("schedulerName") final String schedulerName,
			@PathVariable("group") final String group,
			@PathVariable("name") final String name) {
		try {
			final Scheduler sched = getScheduler(schedulerName);
			final JobKey jobKey = new JobKey(name, group);
			final JobDetail jobDetail = sched.getJobDetail(jobKey);
			if (jobDetail == null) {
				throw new JobNotFoundException(jobKey);
			}

			final Trigger trigger = TriggerBuilder.newTrigger()
					.forJob(jobDetail).startNow().build();
			sched.scheduleJob(trigger);

		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

}
