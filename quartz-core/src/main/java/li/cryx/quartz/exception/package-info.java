/**
 * An implementation of a quartz scheduler can fire checked exception at any
 * time. To give spring a chance to distinguish between all the possible cases,
 * some unchecked exceptions are defined that also include a HTTP status code.
 *
 * <p>
 * You can always define your own error handler on and further refine the
 * exceptions and how they are represented on the REST interface.
 * </p>
 */
package li.cryx.quartz.exception;