package li.cryx.quartz.exception;

import org.quartz.TriggerKey;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import li.cryx.quartz.model.TriggerInfo;

/**
 * An action asks for a specific <code>Trigger</code> or {@link TriggerInfo}
 * that could not be found on the current scheduler.
 *
 * @author cryxli
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class TriggerNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -5797786412709762034L;

	public TriggerNotFoundException(final String triggerName) {
		super("Trigger not found: " + triggerName);
	}

	public TriggerNotFoundException(final String name, final String group) {
		this(name + "." + group);
	}

	public TriggerNotFoundException(final TriggerKey key) {
		this(key.toString());
	}

}