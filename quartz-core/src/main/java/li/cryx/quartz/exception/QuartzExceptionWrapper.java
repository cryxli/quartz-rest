package li.cryx.quartz.exception;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class offers a method to turn a checked <code>SchedulerException</code>
 * into an unchecked {@link WrappedQuartzException} that does contain the stack
 * trace but no dependencies to quartz itself.
 *
 * @author cryxli
 */
public class QuartzExceptionWrapper {

	private static final Logger LOG = LoggerFactory
			.getLogger(QuartzExceptionWrapper.class);

	public static String exceptionToString(final Throwable th) {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (PrintStream ps = new PrintStream(baos, true,
				StandardCharsets.UTF_8.name())) {
			th.printStackTrace(ps);
		} catch (UnsupportedEncodingException e) {
			// Java does not know UTF-8
			LOG.error("Java does not know UTF-8", e);
			LOG.error("Exception", th);
		}
		return new String(baos.toByteArray(), StandardCharsets.UTF_8);
	}

	/**
	 * Re-throw a <code>SchedulerException</code> as a
	 * {@link WrappedQuartzException}.
	 *
	 * @param exception
	 *            A generic exception from quartz.
	 * @return To help the compiler indicate that an exception could be
	 *         returned, when in fact, it is always thrown.
	 * @throws WrappedQuartzException
	 *             Throw the wrapped <code>SchedulerException</code>, always.
	 */
	public static WrappedQuartzException handle(
			final SchedulerException exception) throws WrappedQuartzException {
		throw new WrappedQuartzException(exceptionToString(exception));
	}

}
