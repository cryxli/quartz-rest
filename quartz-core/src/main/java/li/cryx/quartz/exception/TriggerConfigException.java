package li.cryx.quartz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class TriggerConfigException extends RuntimeException {

    private static final long serialVersionUID = 1284969937485578148L;

    public TriggerConfigException(final String msg) {
        super(msg);
    }

    public TriggerConfigException(final String msg, final Throwable th) {
        super(msg, th);
    }

}
