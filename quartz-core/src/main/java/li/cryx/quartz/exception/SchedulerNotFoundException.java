package li.cryx.quartz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * An action asks for a scheduler that couldn't be found.
 *
 * @author cryxli
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class SchedulerNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 2792285646600296811L;

	public SchedulerNotFoundException(final String schedulerName) {
		super("Scheduler not found: " + schedulerName);
	}

}
