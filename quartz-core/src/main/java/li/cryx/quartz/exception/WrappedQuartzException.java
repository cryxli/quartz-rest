package li.cryx.quartz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A checked <code>SchedulerException</code> is packed into an unchecked
 * exception.
 *
 * @author cryxli
 */
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
public class WrappedQuartzException extends RuntimeException {

	private static final long serialVersionUID = 4290819221698936028L;

	WrappedQuartzException(final String msg) {
		super(msg);
	}

}
