package li.cryx.quartz.exception;

import org.quartz.JobKey;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import li.cryx.quartz.model.JobInfo;

/**
 * An action asks for a specific <code>Job</code>, <code>JobDetail</code> or
 * {@link JobInfo} that could not be found on the current scheduler.
 *
 * @author cryxli
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class JobNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -2843785779391533496L;

	public JobNotFoundException(final JobKey key) {
		this(key.toString());
	}

	public JobNotFoundException(final String jobName) {
		super("JobDetail not found: " + jobName);
	}

	public JobNotFoundException(final String name, final String group) {
		this(name + "." + group);
	}

}
