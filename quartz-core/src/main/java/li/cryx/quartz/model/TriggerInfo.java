package li.cryx.quartz.model;

import java.util.Date;

import org.quartz.JobKey;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerKey;

/**
 * POJO containing common properties among all quartz triggers.
 *
 * @author cryxli
 * @since 0.1
 */
public class TriggerInfo {

	private TriggerKey key;

	private JobKey job;

	private TriggerType type = TriggerType.UNKNOWN;

	private Date nextFireTime;

	private Date previousFireTime;

	private String description;

	private TriggerMisfiredInstruction misfiredInstruction = TriggerMisfiredInstruction.SMART_POLICY;

	private String scheduler;

	private TaskState state;

	public String getDescription() {
		return description;
	}

	public JobKey getJob() {
		return job;
	}

	public TriggerKey getKey() {
		return key;
	}

	public TriggerMisfiredInstruction getMisfiredInstruction() {
		return misfiredInstruction;
	}

	public Date getNextFireTime() {
		return nextFireTime;
	}

	public Date getPreviousFireTime() {
		return previousFireTime;
	}

	public String getScheduler() {
		return scheduler;
	}

	public TaskState getState() {
		return state;
	}

	public TriggerType getType() {
		return type;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public void setJob(final JobKey job) {
		this.job = job;
	}

	public void setKey(final TriggerKey key) {
		this.key = key;
	}

	public void setMisfiredInstruction(
			final TriggerMisfiredInstruction misfiredInstruction) {
		this.misfiredInstruction = misfiredInstruction;
	}

	public void setMisfiredInstructionQuartz(final int misfiredInstruction) {
		setMisfiredInstruction(
				TriggerMisfiredInstruction.forValue(misfiredInstruction));
	}

	public void setNextFireTime(final Date nextFireTime) {
		this.nextFireTime = nextFireTime;
	}

	public void setPreviousFireTime(final Date previousFireTime) {
		this.previousFireTime = previousFireTime;
	}

	public void setQuartzState(final TriggerState state) {
		setState(TaskState.forState(state));
	}

	public void setScheduler(final String scheduler) {
		this.scheduler = scheduler;
	}

	public void setState(final TaskState state) {
		this.state = state;
	}

	protected void setType(final TriggerType type) {
		this.type = type;
	}

}
