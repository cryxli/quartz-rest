package li.cryx.quartz.model;

import org.quartz.Trigger.TriggerState;

/**
 * Enum to turn quartz's <code>TriggerState</code> into an independent
 * representation.
 *
 * @author cryxli
 */
public enum TaskState {

	/** Unknown state */
	UNKNOWN,

	/**
	 * Task is not running. Usually this means the trigger is waiting for the
	 * next fire time.
	 */
	SCHEDULED,

	/** Task is being executed. */
	RUNNING,

	/** Task is paused and will not fire until resumed. */
	PAUSED,

	/**
	 * Last execution completed successfully. May wait for another trigger to
	 * fire of never run again.
	 */
	COMPLETE,

	/**
	 * Last execution aborted with an error. May wait for another trigger to
	 * fire of never run again.
	 */
	ERROR;

	/**
	 * Turn <code>TriggerState</code> into {@link TaskState}.
	 *
	 * @param state
	 *            A trigger state from quartz. Can be <code>null</code>.
	 * @return Corresponding state, or, {@link #UNKNOWN}.
	 */
	public static TaskState forState(final TriggerState state) {
		if (state == null) {
			return UNKNOWN;
		}
		switch (state) {
			default :
			case NONE :
				return UNKNOWN;
			case NORMAL :
				return SCHEDULED;
			case BLOCKED :
				return RUNNING;
			case PAUSED :
				return PAUSED;
			case COMPLETE :
				return COMPLETE;
			case ERROR :
				return ERROR;
		}
	}

}
