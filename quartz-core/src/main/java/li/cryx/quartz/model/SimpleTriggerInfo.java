package li.cryx.quartz.model;

import java.util.Date;

/**
 * POJO containing only simple trigger specific information.
 *
 * <p>
 * A <code>SimpleTrigger</code> fires in a set interval.
 * </p>
 *
 * @author cryxli
 * @since 0.1
 */
public class SimpleTriggerInfo extends TriggerInfo {

	/** Timestamp when the trigger would have fired the first time. */
	private Date startTime;

	/**
	 * The interval in milliseconds after which the trigger fires again starting
	 * at {@link #startTime}.
	 */
	private long interval;

	/**
	 * How many times the trigger should fire before it will unregister itself
	 * from quartz.
	 *
	 * If the trigger is not limited to a certain amount of executions, this
	 * value is <code>-1</code>.
	 */
	private int repeatCount;

	// TODO

	public SimpleTriggerInfo() {
		setType(TriggerType.SIMPLE);
	}

	public long getInterval() {
		return interval;
	}

	public int getRepeatCount() {
		return repeatCount;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setInterval(final long interval) {
		this.interval = interval;
	}

	public void setRepeatCount(final int repeatCount) {
		this.repeatCount = repeatCount;
	}

	public void setStartTime(final Date startTime) {
		this.startTime = startTime;
	}

}
