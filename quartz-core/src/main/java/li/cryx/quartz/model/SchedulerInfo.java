package li.cryx.quartz.model;

import java.util.Set;

import org.quartz.JobKey;
import org.quartz.TriggerKey;

/**
 * POJO containing information about a scheduler instance.
 *
 * @author cryxli
 * @since 0.1
 */
public class SchedulerInfo {

	/** Internal name of the trigger */
	private String name;

	/**
	 * ID of the scheduler instance in a cluster setup. One scheduler with a
	 * specific {@link #name} runs multiple times in a cluster. But each
	 * instance of that same scheduler has its very own ID.
	 */
	private String id;

	/**
	 * Indicator that the scheduler is running.
	 *
	 * <code>true</code>, if the scheduler is executing tasks.
	 * <code>false</code>, otherwise.
	 */
	private boolean running;

	/**
	 * Indicator the the scheduler has been shut down. A scheduler that once
	 * enters this state cannot be restarted.
	 *
	 * <code>true</code>, if the scheduler has been shut down.
	 * <code>false</code>, otherwise.
	 */
	private boolean shutdown;

	/** List of existing jobs identified by their group/name. */
	private Set<JobKey> jobs;

	/** List of existing triggers identified by their group/name. */
	private Set<TriggerKey> triggers;

	public String getId() {
		return id;
	}

	public Set<JobKey> getJobs() {
		return jobs;
	}

	public String getName() {
		return name;
	}

	public Set<TriggerKey> getTriggers() {
		return triggers;
	}

	public boolean isRunning() {
		return running;
	}

	public boolean isShutdown() {
		return shutdown;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public void setJobs(final Set<JobKey> jobs) {
		this.jobs = jobs;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setRunning(final boolean running) {
		this.running = running;
	}

	public void setShutdown(final boolean shutdown) {
		this.shutdown = shutdown;
	}

	public void setTriggers(final Set<TriggerKey> triggers) {
		this.triggers = triggers;
	}

}
