package li.cryx.quartz.model;

/**
 * Quartz has a tendency to hide its trigger types. This enum tries to mitigate
 * this.
 *
 * @author cryxli
 */
public enum TriggerType {

	/** Trigger type is unknown. */
	UNKNOWN,

	/** Indicating a <code>CronTrigger</code>. */
	CRON,

	/** Indicating a <code>SimpleTrigger</code>. */
	SIMPLE;

}
