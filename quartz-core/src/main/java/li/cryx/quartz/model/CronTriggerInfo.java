package li.cryx.quartz.model;

/**
 * POJO containing only CRON trigger specific information.
 *
 * <p>
 * A <code>CronTrigger</code> fires whenever the CRON expression evaluates.
 * </p>
 *
 * @author cryxli
 * @since 0.1
 */
public class CronTriggerInfo extends TriggerInfo {

	private String cronExpression;

	public CronTriggerInfo() {
		setType(TriggerType.CRON);
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(final String cronExpression) {
		this.cronExpression = cronExpression;
	}

}
