package li.cryx.quartz.model;

import org.quartz.Job;
import org.quartz.JobKey;

/**
 * POJO to describe a <code>JobDetail</code> of quartz.
 *
 * @author cryxli
 * @since 0.1
 */
public class JobInfo {

	public JobKey key;

	private Class<? extends Job> jobClass;

	private String description;

	private boolean concurrent;

	private boolean durable;

	private String scheduler;

	public String getDescription() {
		return description;
	}

	public Class<? extends Job> getJobClass() {
		return jobClass;
	}

	public JobKey getKey() {
		return key;
	}

	public String getScheduler() {
		return scheduler;
	}

	public boolean isConcurrent() {
		return concurrent;
	}

	public boolean isDurable() {
		return durable;
	}

	public void setConcurrent(final boolean concurrent) {
		this.concurrent = concurrent;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public void setDurable(final boolean durable) {
		this.durable = durable;
	}

	public void setJobClass(final Class<? extends Job> jobClass) {
		this.jobClass = jobClass;
	}

	public void setKey(final JobKey key) {
		this.key = key;
	}

	public void setScheduler(final String scheduler) {
		this.scheduler = scheduler;
	}

}
