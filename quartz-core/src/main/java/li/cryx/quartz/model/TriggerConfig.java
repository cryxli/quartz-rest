package li.cryx.quartz.model;

import java.util.Date;

import org.quartz.JobKey;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerKey;
import org.springframework.util.StringUtils;

import li.cryx.quartz.exception.TriggerConfigException;

public class TriggerConfig {

    private String group;

    private String name;

    private String cron;

    private long interval;

    private Date startTime;

    private int repeatCount = SimpleTrigger.REPEAT_INDEFINITELY;

    private int misfiredInstruction;

    public String getCron() {
        return cron;
    }

    public String getGroup() {
        return group;
    }

    public long getInterval() {
        return interval;
    }

    public TriggerKey getKey() throws TriggerConfigException {
        if (StringUtils.isEmpty(name)) {
            throw new TriggerConfigException("Trigger name cannot be null.");
        } else if (StringUtils.isEmpty(group)) {
            return new TriggerKey(name);
        } else {
            return new TriggerKey(name, group);
        }
    }

    public TriggerKey getKey(final JobKey jobKey) {
        if (StringUtils.isEmpty(name)) {
            throw new TriggerConfigException("Trigger name cannot be null.");
        } else if (StringUtils.isEmpty(group)) {
            return new TriggerKey(name, jobKey.getGroup());
        } else {
            return new TriggerKey(name, group);
        }
    }

    public String getName() {
        return name;
    }

    public int getRepeatCount() {
        return repeatCount;
    }

    public Date getStartTime() {
        return startTime;
    }

    public TriggerType getType() {
        if (!StringUtils.isEmpty(cron)) {
            return TriggerType.CRON;
        } else if (interval > 0 && startTime != null) {
            return TriggerType.SIMPLE;
        } else {
            return TriggerType.UNKNOWN;
        }
    }

    public void setRepeatCount(final int repeatCount) {
        this.repeatCount = repeatCount;
    }

}
