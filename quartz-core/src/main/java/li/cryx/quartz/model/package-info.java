/**
 * Gathering information about quartz entities is done by populating some POJO
 * classes before spring converts them into JSON. This package contains all
 * these POJOs and enumerations.
 * <p>
 * Another goal of these POJO classes is to abstract quartz internals completely
 * from the REST interface.
 * </p>
 */
package li.cryx.quartz.model;