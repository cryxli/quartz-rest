package li.cryx.quartz.model;

public class TaskInfo {

	private JobInfo job;

	private TriggerInfo trigger;

	public JobInfo getJob() {
		return job;
	}

	public TriggerInfo getTrigger() {
		return trigger;
	}

	public void setJob(final JobInfo job) {
		this.job = job;
		updateScheduler();
	}

	public void setScheduler(final String schedulerName) {
		if (getJob() != null) {
			getJob().setScheduler(schedulerName);
		}
		if (getTrigger() != null) {
			getTrigger().setScheduler(schedulerName);
		}
	}

	public void setTrigger(final TriggerInfo trigger) {
		this.trigger = trigger;
	}

	private void updateScheduler() {
		if (getTrigger() != null && getTrigger().getScheduler() != null) {
			setScheduler(getTrigger().getScheduler());
		} else if (getJob() != null && getJob().getScheduler() != null) {
			setScheduler(getJob().getScheduler());
		}
	}

}
