package li.cryx.quartz.builder;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import li.cryx.quartz.exception.JobNotFoundException;
import li.cryx.quartz.exception.QuartzExceptionWrapper;
import li.cryx.quartz.exception.SchedulerNotFoundException;
import li.cryx.quartz.exception.WrappedQuartzException;
import li.cryx.quartz.model.JobInfo;

public class JobInfoBuilder {

	public static JobInfoBuilder fromScheduler(final Scheduler sched) {
		if (sched == null) {
			throw new SchedulerNotFoundException(null);
		}
		return new JobInfoBuilder(sched);
	}

	/** Reference to the scheduler containing the job. */
	private final Scheduler sched;

	/** Identify a job by its key. */
	private JobKey jobKey;

	/** Reference to an actual quartz job. */
	private JobDetail jobDetail;

	private JobInfoBuilder(final Scheduler sched) {
		this.sched = sched;
	}

	public JobInfo build() throws JobNotFoundException, WrappedQuartzException {
		try {
			return innerBuild();
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	private JobInfo innerBuild()
			throws SchedulerException, JobNotFoundException {
		final JobInfo info = new JobInfo();
		info.setScheduler(sched.getSchedulerName());

		if (jobDetail == null) {
			jobDetail = sched.getJobDetail(jobKey);
		}
		if (jobDetail == null) {
			throw new JobNotFoundException(jobKey);
		}

		info.setKey(jobDetail.getKey());
		info.setJobClass(jobDetail.getJobClass());
		info.setDescription(jobDetail.getDescription());
		info.setConcurrent(!jobDetail.isConcurrentExectionDisallowed());
		info.setDurable(jobDetail.isDurable());
		return info;
	}

	public JobInfoBuilder job(final JobDetail jobDetail) {
		this.jobDetail = jobDetail;
		return this;
	}

	public JobInfoBuilder job(final JobKey jobKey) {
		this.jobKey = jobKey;
		return this;
	}

	public JobInfoBuilder job(final String name, final String group) {
		return job(new JobKey(name, group));
	}

}
