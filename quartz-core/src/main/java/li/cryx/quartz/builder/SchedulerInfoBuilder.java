package li.cryx.quartz.builder;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.matchers.GroupMatcher;

import li.cryx.quartz.model.SchedulerInfo;

public class SchedulerInfoBuilder {

	public static SchedulerInfo fromScheduler(final Scheduler sched)
			throws SchedulerException {
		final SchedulerInfo info = new SchedulerInfo();
		info.setName(sched.getSchedulerName());
		info.setShutdown(sched.isShutdown());
		info.setRunning(isRunning(sched));
		info.setId(sched.getSchedulerInstanceId());

		// gather additional info only available when not shut down already
		if (!info.isShutdown()) {
			info.setJobs(sched.getJobKeys(GroupMatcher.anyJobGroup()));
			info.setTriggers(
					sched.getTriggerKeys(GroupMatcher.anyTriggerGroup()));
			// TODO
		}
		return info;
	}

	public static boolean isRunning(final Scheduler sched)
			throws SchedulerException {
		return sched.isStarted() && !sched.isInStandbyMode()
				&& !sched.isShutdown();
	}

}
