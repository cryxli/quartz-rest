package li.cryx.quartz.builder;

import org.quartz.CronTrigger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerKey;

import li.cryx.quartz.exception.QuartzExceptionWrapper;
import li.cryx.quartz.exception.SchedulerNotFoundException;
import li.cryx.quartz.exception.TriggerNotFoundException;
import li.cryx.quartz.exception.WrappedQuartzException;
import li.cryx.quartz.model.CronTriggerInfo;
import li.cryx.quartz.model.SimpleTriggerInfo;
import li.cryx.quartz.model.TriggerInfo;
import li.cryx.quartz.model.TriggerMisfiredInstruction;

public class TriggerInfoBuilder {

	public static TriggerInfoBuilder fromScheduler(final Scheduler sched) {
		if (sched == null) {
			throw new SchedulerNotFoundException(null);
		}
		return new TriggerInfoBuilder(sched);
	}

	public static void setState(final Scheduler sched, final TriggerInfo info)
			throws SchedulerException {
		final TriggerState state = sched.getTriggerState(info.getKey());
		info.setQuartzState(state);
	}

	private final Scheduler sched;

	private TriggerKey triggerKey;

	private Trigger trigger;

	private TriggerInfoBuilder(final Scheduler sched) {
		this.sched = sched;
	}

	public TriggerInfo build()
			throws TriggerNotFoundException, WrappedQuartzException {
		try {
			return innerBuild();
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	private <I extends TriggerInfo> I commonTrigger(final Trigger t,
			final I info) {
		info.setKey(t.getKey());
		info.setJob(t.getJobKey());
		info.setNextFireTime(t.getNextFireTime());
		info.setPreviousFireTime(t.getPreviousFireTime());
		info.setMisfiredInstructionQuartz(t.getMisfireInstruction());
		info.setDescription(t.getDescription());
		return info;
	}

	public CronTriggerInfo fromTrigger(final CronTrigger t) {
		final CronTriggerInfo info = commonTrigger(t, new CronTriggerInfo());
		info.setCronExpression(t.getCronExpression());

		// handle a collision in quartz's misfired instructions
		if (info.getMisfiredInstruction() == TriggerMisfiredInstruction.RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT) {
			info.setMisfiredInstruction(TriggerMisfiredInstruction.DO_NOTHING);
		}
		return info;
	}

	public SimpleTriggerInfo fromTrigger(final SimpleTrigger t) {
		final SimpleTriggerInfo info = commonTrigger(t,
				new SimpleTriggerInfo());
		info.setStartTime(t.getStartTime());
		info.setInterval(t.getRepeatInterval());
		info.setRepeatCount(t.getRepeatCount());

		// handle a collision in quartz's misfired instructions
		if (info.getMisfiredInstruction() == TriggerMisfiredInstruction.DO_NOTHING) {
			info.setMisfiredInstruction(
					TriggerMisfiredInstruction.RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT);
		}
		return info;
	}

	public TriggerInfo fromTrigger(final Trigger t) {
		if (t instanceof CronTrigger) {
			return fromTrigger((CronTrigger) t);

		} else if (t instanceof SimpleTrigger) {
			return fromTrigger((SimpleTrigger) t);

		} else {
			return commonTrigger(t, new TriggerInfo());
		}
	}

	private TriggerInfo innerBuild()
			throws SchedulerException, TriggerNotFoundException {
		final TriggerInfo info;

		if (trigger == null) {
			trigger = sched.getTrigger(triggerKey);
		}
		if (trigger == null) {
			throw new TriggerNotFoundException(triggerKey);
		}
		info = fromTrigger(trigger);

		info.setScheduler(sched.getSchedulerName());
		setState(sched, info);
		return info;
	}

	public TriggerInfoBuilder trigger(final String name, final String group) {
		return trigger(new TriggerKey(name, group));
	}

	public TriggerInfoBuilder trigger(final Trigger trigger) {
		this.trigger = trigger;
		return this;
	}

	public TriggerInfoBuilder trigger(final TriggerKey triggerKey) {
		this.triggerKey = triggerKey;
		return this;
	}

}
