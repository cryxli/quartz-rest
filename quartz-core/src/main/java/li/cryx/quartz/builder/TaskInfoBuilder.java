package li.cryx.quartz.builder;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;

import li.cryx.quartz.exception.JobNotFoundException;
import li.cryx.quartz.exception.QuartzExceptionWrapper;
import li.cryx.quartz.exception.SchedulerNotFoundException;
import li.cryx.quartz.exception.TriggerNotFoundException;
import li.cryx.quartz.exception.WrappedQuartzException;
import li.cryx.quartz.model.JobInfo;
import li.cryx.quartz.model.TaskInfo;
import li.cryx.quartz.model.TriggerInfo;

public class TaskInfoBuilder {

	public static TaskInfoBuilder fromScheduler(final Scheduler sched) {
		if (sched == null) {
			throw new SchedulerNotFoundException(null);
		}
		return new TaskInfoBuilder(sched);
	}

	private final Scheduler sched;

	private TriggerKey triggerKey;

	private TriggerInfo triggerInfo;

	private JobInfo jobInfo;

	private TaskInfoBuilder(final Scheduler sched) {
		this.sched = sched;
	}

	public TaskInfo build() throws TriggerNotFoundException,
			JobNotFoundException, WrappedQuartzException {
		try {
			return innerBuild();
		} catch (SchedulerException e) {
			throw QuartzExceptionWrapper.handle(e);
		}
	}

	private TaskInfo innerBuild() throws SchedulerException,
			TriggerNotFoundException, JobNotFoundException {
		final TaskInfo info = new TaskInfo();

		if (triggerInfo == null) {
			triggerInfo = TriggerInfoBuilder.fromScheduler(sched)
					.trigger(triggerKey).build();
		} else {
			// try to get really latest state
			TriggerInfoBuilder.setState(sched, triggerInfo);
		}
		info.setTrigger(triggerInfo);

		if (jobInfo == null) {
			jobInfo = JobInfoBuilder.fromScheduler(sched)
					.job(info.getTrigger().getJob()).build();
		}
		info.setJob(jobInfo);

		info.setScheduler(sched.getSchedulerName());
		return info;
	}

	public TaskInfoBuilder job(final JobInfo jobInfo) {
		this.jobInfo = jobInfo;
		return this;
	}

	public TaskInfoBuilder trigger(final String name, final String group) {
		return trigger(new TriggerKey(name, group));
	}

	public TaskInfoBuilder trigger(final Trigger trigger)
			throws SchedulerException {
		return trigger(TriggerInfoBuilder.fromScheduler(sched).trigger(trigger)
				.build());
	}

	public TaskInfoBuilder trigger(final TriggerInfo triggerInfo) {
		this.triggerInfo = triggerInfo;
		return this;
	}

	public TaskInfoBuilder trigger(final TriggerKey triggerKey) {
		this.triggerKey = triggerKey;
		return this;
	}

}
