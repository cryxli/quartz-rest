/**
 * This package contains builders or factories to turn quartz entities into
 * POJOs.
 *
 * @see li.cryx.quartz.model
 */
package li.cryx.quartz.builder;